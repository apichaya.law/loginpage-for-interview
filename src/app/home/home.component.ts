import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'loginpage-panelplus-interview'; // title เป็นตัวแปรในหน้า home.html มันจะมาดึงค่าใน ' ' ไปแสดงผล

  constructor() { }

  ngOnInit(): void {
  }

}
